#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:39:19 2020

@author: antoine
"""
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget

from model.list_model import ListModel


class ListView(QWidget):
    def __init__(self, studb, list_obj):
        QWidget.__init__(self)
        uic.loadUi('./view/listview_widget.ui', self)  # Load the .ui file

        self.dtb = studb

#        self.list_obj = list_obj
#        print(self.list_obj)
#        self.list_model = ListModel(self.dtb, self.list_obj)
#        self.listView.setModel(self.list_model)
        self.listView.setModel(ListModel(self.dtb, list_obj))

    def get_obj(self):
        try:
            if self.listView.currentIndex().row() != -1:
                return self.listView.model().get_items()[
                    self.listView.currentIndex().row()]
            else:
                return None
        except BaseException:
            return None

    def set_obj(self, obj=None):
        if (obj is not None) and (obj in self.listView.model().get_items()):
            position = self.listView.model().get_items().index(obj)
            index = self.listView.model().index(position)
            self.listView.setCurrentIndex(index)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("sample/demo.sqlite")
    widget = ListView(dtb, dtb.table_breed.get_breeds)
    widget.show()
    sys.exit(app.exec_())
