#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 19:56:29 2020

@author: antoine
"""

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout

from controller.horse_description_widget import HorseDescriptionWidget
from controller.table_health_record_view import TableHealthRecordView
from controller.table_movement_record_view import TableMovementRecordView


class HorseWidget(QWidget):
    "widget to describe a horse"

    def __init__(self, studdb, horse=None):
        QWidget.__init__(self)
        uic.loadUi('./view/horse_widget.ui', self)  # Load the .ui file
        self.dtb = studdb
        self.horse = horse

        if self.horse is None:
            self.tabWidget.setEnabled(False)

        self.horse_description = HorseDescriptionWidget(self.dtb, self.horse)
        QVBoxLayout(self.tabDescription).addWidget(self.horse_description)
        self.health_record_description = TableHealthRecordView(
            self.dtb, self.horse)
        QVBoxLayout(
            self.tabHealthRecords).addWidget(
            self.health_record_description)
        self.movement_record_description = TableMovementRecordView(
            self.dtb, self.horse)
        QVBoxLayout(
            self.tabMovementRecords).addWidget(
            self.movement_record_description)

        self.update_horse_name_title()
        self.horse_description.line_name.textChanged.connect(
            self.update_horse_name_title)

    def update_horse_name_title(self):
        "update the horse name title"
        if self.horse is not None:
            horse_name = self.horse_description.line_name.text()
            self.label.setText("<h1>" + horse_name + "</h1>")
        else:
            self.label.setText("<h1>" + "" + "</h1>")

    def set_horse(self, horse):
        self.horse = horse
        if self.horse is not None:
            self.tabWidget.setEnabled(True)
        else:
            self.tabWidget.setEnabled(False)

        self.horse_description.set_horse(self.horse)
        self.health_record_description.set_horse(self.horse)
        self.movement_record_description.set_horse(self.horse)
        self.update_horse_name_title()


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb
    from model.studdb import Horse

    app = QApplication(sys.argv)
    dtb = StudDb("sample/demo.sqlite")
    #horse = dtb.table_horse.get_horses()[1]
    horse = Horse(dtb, 1)
    widget = HorseWidget(dtb, horse)
    widget.show()
    sys.exit(app.exec_())
