#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 18:53:36 2020

@author: antoine
"""

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QMessageBox

from view.list_view import ListView
from model.horse_model import HorseModel


class HorseView(ListView):

    def __init__(self, studb):
        QWidget.__init__(self)
        uic.loadUi('./view/listview_widget.ui', self)  # Load the .ui file
        self.dtb = studb

        # on hérite de ListView
        ListView.__init__(self, self.dtb, self.dtb.table_horse.get_horses)
        # on écrase son model
#        self.list_model = HorseModel(self.dtb)
#        self.listView.setModel(self.list_model)
        self.listView.setModel(HorseModel(self.dtb))

        self.pushButtonAdd.clicked.connect(self.add)
        self.pushButtonDelete.clicked.connect(self.delete)

    def add(self):
        horse = self.listView.model().add_horse()
        self.set_obj(horse)

    def delete(self):
        horse = self.get_obj()
        if horse is None:
            QMessageBox.warning(
                self, self.tr('Error !'),
                self.tr("You have to select one item to delete."),
                QMessageBox.Ok)
        else:
            try:
                self.listView.model().remove_horse(horse)
                self.set_obj(None)
            except BaseException:
                QMessageBox.warning(
                    self, self.tr('Error !'),
                    self.tr("This item is related to others operations and can not be deleted."),
                    QMessageBox.Ok)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb

    dtb = StudDb("sample/demo.sqlite")
    #widget = ContactModel(dtb)

    app = QApplication(sys.argv)
    widget = HorseView(dtb)
    widget.show()
    sys.exit(app.exec_())
