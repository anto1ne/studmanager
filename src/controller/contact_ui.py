#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 09:49:00 2020

@author: antoine
"""

import sys
from PyQt5.QtWidgets import QWidget


from controller.contact_list import ContactView
from controller.contact_description_widget import ContactDescriptionWidget

from PyQt5.QtWidgets import QHBoxLayout


class ContactUI(QWidget):
    "widget inside description contact tab"

    def __init__(self, studdb, contact=None):
        QWidget.__init__(self)
        self.dtb = studdb

        self.view = ContactView(self.dtb)
        self.description = ContactDescriptionWidget(self.dtb)

        # creates the horizontal box layout
        hlayout = QHBoxLayout(self)

        # adds the buttons to the layout
        hlayout.addWidget(self.view, 2)
        hlayout.addWidget(self.description, 8)

        self.setLayout(hlayout)

        # signal
        self.view.listView.selectionModel().currentChanged.connect(self.update_description)
        self.view.pushButtonAdd.clicked.connect(self.update_description)
        self.view.pushButtonDelete.clicked.connect(self.update_description)

    def update_description(self):
        contact = self.view.get_obj()
        self.description.set_contact(contact)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    #contact = dtb.table_contact.get_contacts()[0]
    widget = ContactUI(dtb)
    widget.show()
    sys.exit(app.exec_())
