#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import QDate


class InputHealthRecord(QDialog):
    "dialog to create/edit movement records"
#            self.setWindowIcon(QIcon(ICON_PATH + "movements.svg"))

    def __init__(self, studdb, health_record=None, parent=None):
        # Call the inherited classes __init__ method
        super(InputHealthRecord, self).__init__(parent)
        uic.loadUi('./view/health_record_dialog.ui', self)  # Load the .ui file

        self.dtb = studdb
        self.comboBox.update(
            self.dtb,
            self.dtb.table_contact.get_contacts,
            obj=None)
        self.set_infos(health_record)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def set_infos(self, health_record):
        # data to display
        if health_record is None:
            date = QDate.currentDate()
            comments = None
            contact = None
        else:
            date = health_record.get_date()
            comments = health_record.get_comments()
            contact = health_record.get_contact()

        # widget
        self.dateEdit.setDate(date)
        if comments:
            self.plainTextEdit.setPlainText(comments)
        self.comboBox.set_obj(contact)

    def get_infos(self):
        "return infos from dialog"
        date_health_record = self.dateEdit.date().toString("yyyy-MM-dd")
        comments = self.plainTextEdit.toPlainText()
        contact = self.comboBox.get_obj()
        return date_health_record, contact, comments


if __name__ == '__main__':
    from horsedb import HealthRecord
    from PyQt5.QtWidgets import QApplication
    from studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    hr = HealthRecord(dtb, 1)
    widget = InputHealthRecord(dtb, hr)

    if widget.exec_() == QDialog.Accepted:
        print(widget.get_infos())

    # widget.show()
    # sys.exit(app.exec_())
