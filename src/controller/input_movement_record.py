#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import QDate


class InputMovementRecord(QDialog):
    "dialog to create/edit movement records"
#            self.setWindowIcon(QIcon(ICON_PATH + "movements.svg"))

    def __init__(self, studdb, movement_record=None, parent=None):
        # Call the inherited classes __init__ method
        super(InputMovementRecord, self).__init__(parent)
        uic.loadUi(
            './view/movement_record_dialog.ui',
            self)  # Load the .ui file

        self.dtb = studdb
        self.set_infos(movement_record)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def set_infos(self, movement_record):
        # data to display
        if movement_record is None:
            date = QDate.currentDate()
            comments = None
        else:
            date = movement_record.get_date()
            comments = movement_record.get_comments()

        # widget
        self.dateEdit.setDate(date)
        if comments:
            self.plainTextEdit.setPlainText(comments)

    def get_infos(self):
        "return infos from dialog"
        date_movement_record = self.dateEdit.date().toString("yyyy-MM-dd")
        comments = self.plainTextEdit.toPlainText()
        return date_movement_record, comments


if __name__ == '__main__':
    from horsedb import MovementRecord
    from PyQt5.QtWidgets import QApplication
    from studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    mr = MovementRecord(dtb, 1)
    widget = InputMovementRecord(dtb, mr)

    if widget.exec_() == QDialog.Accepted:
        print(widget.get_infos())

    # widget.show()
    # sys.exit(app.exec_())
