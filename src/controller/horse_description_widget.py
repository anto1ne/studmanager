#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 16:50:04 2020

@author: antoine
"""

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import QDate


class HorseDescriptionWidget(QWidget):
    "widget to describe a horse"

    def __init__(self, studdb, horse=None):
        QWidget.__init__(self)
        uic.loadUi(
            './view/horse_description_widget.ui',
            self)  # Load the .ui file

        self.dtb = studdb
        self.horse = horse

        # init combobox
        self.cb_breed.update(
            self.dtb,
            self.dtb.table_breed.get_breeds,
            obj=None)
        self.cb_color.update(
            self.dtb,
            self.dtb.table_color.get_colors,
            obj=None)
        self.cb_sex.update(self.dtb, self.dtb.table_sex.get_sexes, obj=None)

        # set horse
        self.set_horse(self.horse)

        # signals
        # mise à jour par pushbutton (supprimé)
        # self.pushButton.clicked.connect(self.save_changes)
        # mise à jour auto
        self.line_name.editingFinished.connect(self.save_changes)
        self.dateEdit.dateChanged.connect(self.save_changes)
        self.cb_breed.currentIndexChanged.connect(self.save_changes)
        self.cb_color.currentIndexChanged.connect(self.save_changes)
        self.cb_sex.currentIndexChanged.connect(self.save_changes)

    def save_changes(self):
        "update horse data"
        name = self.line_name.text()
        breed = self.cb_breed.get_obj()
        color = self.cb_color.get_obj()
        sex = self.cb_sex.get_obj()

        birthdate = self.dateEdit.date().toString("yyyy-MM-dd")
        print(name, breed, color, sex, birthdate)
#        print(breed in self.dtb.table_breed.get_breeds())

        self.dtb.table_horse.update_horse(
            self.horse.dtb_id,
            name,
            sex.dtb_id,
            color.dtb_id,
            birthdate,
            breed.dtb_id)

    def set_horse(self, horse=None):
        "set horse to display"

        self.line_name.blockSignals(True)
        self.dateEdit.blockSignals(True)
        self.cb_breed.blockSignals(True)
        self.cb_color.blockSignals(True)
        self.cb_sex.blockSignals(True)

        self.horse = horse
        # set horse in description tab
        if self.horse is not None:
            self.setEnabled(True)
            self.line_name.setText(self.horse.get_name())
            dob = self.horse.get_birthdate()
            self.dateEdit.setDate(QDate(dob.year, dob.month, dob.day))

            self.cb_breed.set_obj(self.horse.get_breed())
            self.cb_color.set_obj(self.horse.get_color())
            self.cb_sex.set_obj(self.horse.get_sex())

        else:
            self.setEnabled(False)

        self.line_name.blockSignals(False)
        self.dateEdit.blockSignals(False)
        self.cb_breed.blockSignals(False)
        self.cb_color.blockSignals(False)
        self.cb_sex.blockSignals(False)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb
    from model.studdb import Horse

    app = QApplication(sys.argv)
    dtb = StudDb("sample/demo.sqlite")
    #horse = dtb.table_horse.get_horses()[1]
    horse1 = Horse(dtb, 1)
    horse2 = Horse(dtb, 2)
    # OK
    widget = HorseDescriptionWidget(dtb, horse1)
    # OK
#    widget = HorseDescriptionWidget(dtb, None)
#    widget.set_horse(horse1)
    # OK
#    widget = HorseDescriptionWidget(dtb, horse2)
#    widget.set_horse(horse1)

    widget.show()
    sys.exit(app.exec_())
