#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:39:19 2020

@author: antoine
"""
import sys
import os
from PyQt5 import uic, QtCore
from PyQt5.QtWidgets import QMainWindow, QWidget, QGridLayout, QStyle
from PyQt5.QtWidgets import QMessageBox, QFileDialog
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QDesktopServices

from model.constants import __version__
from view.centralwidget import CentraWidget
from model.studdb import StudDb

translate = QtCore.QCoreApplication.translate


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        uic.loadUi('./view/main_window.ui', self)  # Load the .ui file

        self.dtb = None
        self.filename = None

        # File Menu
        self.action_New.triggered.connect(self.new_call)
        self.action_Open.triggered.connect(self.open_call)
        self.action_Save.triggered.connect(self.save_call)
        self.action_Save.setEnabled(False)
        self.action_Close.triggered.connect(self.close_call)
        self.action_Quit.triggered.connect(self.exit_call)

        # Help Menu
        self.action_About_StudManager.triggered.connect(self.about_call)
        self.action_Report_a_bug.triggered.connect(self.bug_report_link_call)
        self.action_Website.triggered.connect(self.website_link_call)
        self.action_About_Qt.triggered.connect(self.about_qt_call)

        # icon
        self.action_About_Qt.setIcon(self.style().standardIcon(
            QStyle.SP_TitleBarMenuButton))

    def init_ui(self):
        "init ui"
        self.central_widget = QWidget(self)
        gridLayout = QGridLayout(self.central_widget)
        gridLayout.setContentsMargins(10, 10, 10, 10)
        gridLayout.addWidget(CentraWidget(self.dtb, None))
        self.central_widget.setLayout(gridLayout)
        self.setCentralWidget(self.central_widget)

    def save_before_exit(self):
        answer = QMessageBox.question(
            self,
            translate(
                "mainwindow",
                'Save before exit ?'),
            translate(
                "mainwindow",
                "Do you want to save before exit ?"),
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No)
        if answer == QMessageBox.Yes:
            if self.filename == ":memory:":
                self.set_filename_dialog()
            self.dtb.save()

    def closeEvent(self, event):
        print("User has clicked the red x on the main window")
        if self.dtb is not None:
            self.save_before_exit()
            self.dtb.close()
            self.dtb = None
        event.accept()

    def set_filename_dialog(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _file_type = QFileDialog.getSaveFileName(
            self,
            translate("mainwindow", "New file"),
            "",
            "Sqlite Files (*.sqlite);;All Files (*)",
            options=options)
        if file_name:
            name, extension = os.path.splitext(file_name)
            if extension == ".sqlite":
                self.filename = file_name

            else:
                self.filename = file_name + ".sqlite"
            self.set_filename(self.filename)

    # File Menu
    def new_call(self):
        "to start a new file"
        if self.dtb is not None:
            self.save_before_exit()
            self.dtb.close()
        self.set_filename(":memory:")
        self.action_Save.setEnabled(True)

    def open_call(self):
        "to open a file"
        if self.dtb is not None:
            self.save_before_exit()
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _file_type = QFileDialog.getOpenFileName(
            self,
            translate("mainwindow", "Open file"),
            "",
            "Sqlite Files (*.sqlite);;All Files (*)",
            options=options)
        if file_name:
            self.set_filename(file_name)
            self.action_Save.setEnabled(True)

    def save_call(self):
        "to save file"
        if self.dtb is not None:
            if self.filename == ":memory:":
                self.set_filename_dialog()
            if self.filename != ":memory:":
                self.dtb.save()
                self.statusbar.showMessage(
                    translate(
                        "mainwindow",
                        "File saved successfully"),
                    2000)
        else:
            self.statusbar.showMessage(
                translate(
                    "mainwindow",
                    "No file to save !"),
                2000)

    def close_call(self):
        "to close file"
        if self.dtb is not None:
            self.save_before_exit()
            self.dtb.close()
            del self.dtb
            self.dtb = None
            self.action_Save.setEnabled(False)

            if hasattr(self, "central_widget"):
                # self.main_tabs.setHidden(True)
                self.central_widget.deleteLater()
                self.statusbar.showMessage(
                    translate(
                        "mainwindow",
                        "File closed successfully"),
                    2000)
        else:
            self.statusbar.showMessage(
                translate(
                    "mainwindow",
                    "No file to close !"),
                2000)

    def exit_call(self):
        "to close the window"
        if self.dtb is not None:
            self.save_before_exit()
            self.dtb.close()
            self.dtb = None
        self.close()

    # Help Menu
    def about_call(self):
        "about dialog"
        LICENSE_PATH = "./data/LICENSE"
        licence_file = open(LICENSE_PATH, "r")
        licence = licence_file.read()
        licence_file.close()

        msg = QMessageBox(self)
        msg.setIcon(QMessageBox.Information)

        msg.setText(
            translate(
                "mainwindow",
                "StudManager {} \nStud Manager is a software designed to help you to manage your stud.").format(__version__))
        msg.setInformativeText(
            translate(
                "mainwindow",
                """This software has been written with Python, PyQt5 and sqlite."""))
        msg.setWindowTitle(translate("mainwindow", "About StudManager"))
        msg.setDetailedText(licence)
        msg.setStandardButtons(QMessageBox.Ok)
        # msg.buttonClicked.connect(msgbtn)

        msg.exec_()

    def bug_report_link_call(self):
        "open browser to report bug"
        url = QtCore.QUrl('https://framagit.org/anto1ne/studmanager/issues')
        if not QDesktopServices.openUrl(url):
            QMessageBox.warning(
                self, translate(
                    "mainwindow", "Open Url"), translate(
                    "mainwindow", "Could not open url"))
        pass

    def website_link_call(self):
        "open browser to report bug"
        url = QtCore.QUrl('https://studmanager.frama.io/')
        if not QDesktopServices.openUrl(url):
            QMessageBox.warning(
                self, translate(
                    "mainwindow", "Open Url"), translate(
                    "mainwindow", "Could not open url"))
        pass

    def about_qt_call(self):
        "about qt dialog"
        QMessageBox.aboutQt(self, translate("mainwindow", "About Qt"))

    def set_filename(self, filename):
        "to set the file to read"
        self.filename = filename
        self.dtb = StudDb(filename)

        self.init_ui()


if __name__ == '__main__':
    #    from PyQt5.QtWidgets import QApplication
    #    from studdb import StudDb
    #    from horsedb import Horse

    app = QApplication(sys.argv)
    #dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
#    dtb = StudDb("/document/Docs/depot_git/StudManager/registre_maison/registre.sqlite")
#    horse = Horse(dtb, 1)
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec_())
