#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 09:49:00 2020

@author: antoine
"""

import sys
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QHBoxLayout


from controller.horse_list import HorseView
from controller.horse_widget import HorseWidget


class HorseUI(QWidget):
    "widget inside description contact tab"

    def __init__(self, studdb, horse=None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse

        self.view = HorseView(self.dtb)
        self.description = HorseWidget(self.dtb, self.horse)

        # creates the horizontal box layout
        hlayout = QHBoxLayout(self)

        # adds the buttons to the layout
        hlayout.addWidget(self.view, 2)
        hlayout.addWidget(self.description, 8)

        self.setLayout(hlayout)

        self.view.listView.selectionModel().currentChanged.connect(self.update_description)
        self.view.pushButtonAdd.clicked.connect(self.update_description)
        self.view.pushButtonDelete.clicked.connect(self.update_description)

    def update_description(self):
        self.horse = self.view.get_obj()
        #print("=", self.horse.get_name(), self.horse.get_breed(), self.horse.get_sex())
        self.description.set_horse(self.horse)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb
    from model.studdb import Horse

    app = QApplication(sys.argv)
    dtb = StudDb("sample/demo.sqlite")
    #contact = dtb.table_contact.get_contacts()[0]
    horse = Horse(dtb, 1)
    widget = HorseUI(dtb, None)
    widget.show()
    sys.exit(app.exec_())
