#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 16:30:01 2020

@author: antoine
"""
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget


class ContactDescriptionWidget(QWidget):
    "widget inside description contact tab"

    def __init__(self, studdb, contact=None):
        QWidget.__init__(self)
        uic.loadUi(
            './view/contact_description_widget.ui',
            self)  # Load the .ui file
        self.groupBox.setEnabled(False)

        self.dtb = studdb
        self.contact = contact

        if contact is not None:
            self.groupBox.setEnabled(True)
            self.set_contact(self.contact)

        # signal
        # self.pushButton.clicked.connect(self.save_changes)
        self.line_last_name.editingFinished.connect(self.save_changes)
        self.line_first_name.editingFinished.connect(self.save_changes)
        self.line_organization.editingFinished.connect(self.save_changes)
        self.line_email.editingFinished.connect(self.save_changes)
        self.line_tel.editingFinished.connect(self.save_changes)
        self.line_address.textChanged.connect(self.save_changes)

    def save_changes(self):
        "update data"

        last_name = self.line_last_name.text()
        first_name = self.line_first_name.text()
        organization = self.line_organization.text()
        email = self.line_email.text()
        tel = self.line_tel.text()
        address = self.line_address.toPlainText()
        # print(last_name,first_name,email,tel)
        # print(address)

        self.dtb.table_contact.update_contact(self.contact.dtb_id, last_name,
                                              first_name, address, tel, email,
                                              organization)
#

    def set_contact(self, contact):
        "set contact data in the widget"

        self.line_last_name.blockSignals(True)
        self.line_first_name.blockSignals(True)
        self.line_organization.blockSignals(True)
        self.line_email.blockSignals(True)
        self.line_tel.blockSignals(True)
        self.line_address.blockSignals(True)

        self.contact = contact
        if self.contact is not None:
            self.groupBox.setEnabled(True)
            # set contact in description tab
            self.line_last_name.setText(self.contact.get_last_name())
            self.line_first_name.setText(self.contact.get_first_name())
            self.line_organization.setText(self.contact.get_organization())
            self.line_email.setText(self.contact.get_email())
            self.line_tel.setText(self.contact.get_tel())
            self.line_address.setPlainText(self.contact.get_address())
        else:
            self.groupBox.setEnabled(False)
            self.line_last_name.setText("")
            self.line_first_name.setText("")
            self.line_organization.setText("")
            self.line_email.setText("")
            self.line_tel.setText("")
            self.line_address.setPlainText("")

        self.line_last_name.blockSignals(False)
        self.line_first_name.blockSignals(False)
        self.line_organization.blockSignals(False)
        self.line_email.blockSignals(False)
        self.line_tel.blockSignals(False)
        self.line_address.blockSignals(False)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    contact = dtb.table_contact.get_contacts()[0]
    widget = ContactDescriptionWidget(dtb, contact)
    widget.show()
    sys.exit(app.exec_())
