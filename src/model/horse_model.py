#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 18:53:36 2020

@author: antoine
"""

from PyQt5.QtCore import QModelIndex
import random

from model.list_model import ListModel
from model.horsedb import Horse


class HorseModel(ListModel):

    def __init__(self, studb, parent=None):
        self.dtb = studb
        ListModel.__init__(
            self,
            self.dtb,
            self.dtb.table_horse.get_horses,
            parent)

    def add_horse(self):
        #indice = len(self.get_items())
        indice = random.randint(0, 1000)

        name = self.tr("New horse {}").format(str(indice))
        # print(name)
        id_sex = 1
        id_color = 1
        birthdate = "2020-01-01"
        id_breed = 1

        self.beginInsertRows(QModelIndex(), indice, indice)
        id_horse = self.dtb.table_horse.add_horse(name, id_sex, id_color,
                                                  birthdate, id_breed)
        self.endInsertRows()

        return Horse(self.dtb, id_horse)

    def remove_horse(self, horse):
        print(horse)
        #indice = contact.dtb_id
        # self.beginRemoveRows(QModelIndex(),indice)
        print("suppression")
        self.dtb.table_horse.remove_horse(horse.dtb_id)
        # self.endRemoveRows()
        self.layoutChanged.emit()
