#!/usr/bin/python3
# -*- coding: utf-8 -*-


from PyQt5.QtCore import QAbstractListModel, QModelIndex, Qt


class ListModel(QAbstractListModel):

    def __init__(self, studb, items, parent=None):
        QAbstractListModel.__init__(self, parent)
        self.dtb = studb
        self.items = items

    def get_items(self):
        return self.items()

    def rowCount(self, parent=QModelIndex()):
        return len(self.get_items())

    def data(self, index, role):
        row = index.row()
        if (index.isValid()) and (role == Qt.DisplayRole):
            return self.get_items()[row].get_name()
        else:
            return None


if __name__ == '__main__':
    pass
    #listmodel = ListModel([obj1,obj2,obj3])
