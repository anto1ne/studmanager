#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class to describe Reminder
"""

from datetime import date

from model.horsedb import Horse


class Reminder():
    "class to represent reminder"

    def __init__(self, dtb, id_r):
        self.dtb_id = id_r
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_date(self):
        "return reminder date"
        self.dtb.cur.execute('''select TReminders.date from TReminders
                         where TReminders.id= ? ''', (self.dtb_id, ))
        mr_date = self.dtb.cur.fetchone()[0]
        mr_date_split = mr_date.split('-')
        return date(
            int(mr_date_split[0]), int(mr_date_split[1]), int(
                mr_date_split[2]))

    def get_horse(self):
        "return horse"
        self.dtb.cur.execute('''select TReminders.id_horse from TReminders
                            where TReminders.id= ? ''', (self.dtb_id, ))
        id_horse = self.dtb.cur.fetchone()[0]
        return Horse(self.dtb, id_horse)

    def get_comments(self):
        "return comments"
        self.dtb.cur.execute('''select TReminders.comments from TReminders
                            where TReminders.id= ? ''', (self.dtb_id, ))
        comments = self.dtb.cur.fetchone()[0]
        return comments

    def is_done(self):
        "return reminder state"
        self.dtb.cur.execute('''select TReminders.is_done from TReminders
                            where TReminders.id= ? ''', (self.dtb_id, ))
        is_done = self.dtb.cur.fetchone()[0]
        return bool(is_done)

    def is_done_str(self):
        "returns x when True and '' when false"
        if self.is_done():
            return "x"
        else:
            return ""

    def is_late_not_done(self):
        "return True if reminder is late and not done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=0 and TReminders.date<=date('now')
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        list_id = [item[0] for item in list_id_fr]
        return self.dtb_id in list_id

    def __repr__(self):
        return "==\n{}\n{} \n{}\n{}\n==".format(self.get_date(),
                                                self.get_horse().get_name(),
                                                self.get_comments(),
                                                self.is_done())
