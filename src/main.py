#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Script to start StudManager
"""

import sys
import os

from PyQt5.QtCore import QLocale
from PyQt5 import QtWidgets
from PyQt5.Qt import PYQT_VERSION_STR
from PyQt5.QtCore import QTranslator, QLibraryInfo

import sqlite3
import platform
from model.constants import __version__

from controller.mainwindow import MainWindow

LOCALE = QLocale.system().name()
LOCALE_PATH = "./locale" + os.path.sep + LOCALE + ".qm"


def studmanager():
    "starts StudManager"

    print("Welcome in StudManager !")
    print("==================================================================")
    print("Script name : ", os.path.basename(__file__))
    print("Directory name : ", os.path.dirname(__file__))
    print("Path : ", os.path.abspath(__file__))
    print("==================================================================")
    print("= OS :")
    print(platform.platform())
    print("==================================================================")
    print("= Python :")
    print(sys.version)
    print("= studmanagerlib : ")
    print(__version__)
    print("= PyQt5 : ")
    print(PYQT_VERSION_STR)
    print("= sqlite3 : ")
    print(sqlite3.version)
    print("==================================================================")
    # PYTHONPATH
    print("= PYTHONPATH")
    print("\n".join(sys.path))
#    print("==================================================================")
#    print("ICON PATH : ", ICON_PATH)
#    print("LICENSE PATH : ", LICENSE_PATH)
    print("LOCALE PATH : ", LOCALE_PATH)
    print("==================================================================")
    print("Starting user interface")

    APP = QtWidgets.QApplication(sys.argv)

    translator_qt = QTranslator()
    translator_qt.load(
        "qt_" + LOCALE,
        QLibraryInfo.location(
            QLibraryInfo.TranslationsPath))

    translator_app = QTranslator()
    translator_app.load(LOCALE_PATH)

    APP.installTranslator(translator_qt)
    APP.installTranslator(translator_app)

    MAINWIN = MainWindow()
    MAINWIN.show()
    sys.exit(APP.exec_())


if __name__ == "__main__":
    studmanager()
