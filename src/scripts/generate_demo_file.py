#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Script to generate demo.sqlite in sample/
"""

import os

from model.studdb import StudDb, Contact


def demo_file():
    "generates demo.sqlite in sample/"

    filename = "sample/demo.sqlite"
    print("=== Generates demo.sqlite ===")
    print(filename)

    if os.path.exists(filename):
        print("File already exists")
        print("Removing current file")
        os.remove(filename)

    dtb = StudDb(filename)

    t_contact = dtb.table_contact
    t_contact.add_contact(
        "Wayne",
        "John",
        u"rue du désert\n34000 Montpellier",
        "",
        "john@wayne.fr",
        "")
    id_lucky = t_contact.add_contact(
        "Lucky",
        "Luke",
        "Far West\n59000 Lille",
        "01 00 22 33 44",
        "lucky@luke.com",
        "Ranch")

    manager = Contact(dtb, id_lucky)
    t_infos = dtb.table_infos
    t_infos.set_manager(manager)
    #t_infos.set_version("test version")

    t_horse = dtb.table_horse
    id_jollyjumper = t_horse.add_horse("Jolly Jumper", 2, 10, "2010-04-19", 15)
    t_horse.add_horse("Sea Bird", 1, 11, "2013-09-12", 12)
    t_horse.add_horse("Donkey", 3, 4, "2002-02-13", 9)

    t_healthrecords = dtb.table_health_records
    t_healthrecords.add_health_record(
        "2018-06-12", id_lucky, id_jollyjumper, "Vaccination")

    t_movementrecords = dtb.table_movement_records
    t_movementrecords.add_movement_record(
        "2015-05-27", id_jollyjumper, "Arrived from SuperStud")

    t_reminders = dtb.table_reminders
    t_reminders.add_reminder("2017-06-12", id_jollyjumper, "Vaccination", 1)
    t_reminders.add_reminder("2019-06-12", id_jollyjumper, "Vaccination", 0)
    t_reminders.add_reminder("2025-06-12", id_jollyjumper, "Deworming", 0)

    dtb.save()
    dtb.close()


if __name__ == "__main__":
    demo_file()
